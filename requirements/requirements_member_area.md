## Group Project - Member Area Module

**Assigned to**: Elaine Cochran

Requirements: Need a module which:

**1.** Loads the html content for the member area into the div#member_area of the app's index.html page.

**2.** Exports a function which takes the access module as a parameter and:  
    
- if the user is logged in, displays the member content, showing the user's name as being logged in.  

- if the user is not logged in, the member area content is hidden.

**Note**: Member Area content area should be hidden initially.