## Group Project - Login Screen Module

**Assigned To** : Immanuel Sun

**Requirements** : Need a module which exports an object with the following methods:

**`init`**  -  method which initializes the login screen for the main application:

* loads the html template for the login screen to div#login in the app's index.html page.

* assigns the *`btnLogin_click`* handler to the login button. 

* assigns the *`btnLogout_click`* handler to the logout button.


**`btnLogin_click`** - click handler method which accepts username and password as parameters and:

* calls access.login method with those same parameters to authenticate user.

* calls showMemberArea with the access object.


**`btnLogout_click`** - click handler method which:

* calls access.logout method to log user out of application.

* calls showMemberArea with the access object.


**login screen template** - html which must have the following items:

- text input for username (#username)

- text input for password (#password)

- login button (#btnLogin)

- logout button (#btnLogout)


**Note :**  This module must be testable. One way to make the module testable is to create the module object from a factory method which:

- accepts its dependencies as parameters to make these functions testable using mocks.

- exposes the button click handlers in the public api for testing.