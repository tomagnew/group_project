## Group Project - Access Module

**Assigned To** : Cliff Hales

**Requirements** : Need a module which exports an object with the following properties and methods:

**`loggedIn`** - property (boolean): The current user is logged in (default is no).  
  

**`currentUser`** - custom object which represents the current user:  

- If the user **is not** not logged in, *currentUser* should be null.  
- If the user **is** logged in, *currentUser* should be an object containing at least the user's name.  
  
  
**`login`** - method which accepts username and password as parameters and: 

- verifies these credentials against a list/database.  
- if the user is authenticated, the *`loggedIn`* property is set to true, and the currentUser property is set to an object containing the users info.  
- otherwise, *`loggedIn`* is set to false and *`currentUser`* is set to null.  
  
  
**`logout`** - method which simply:

- sets the *`loggedIn`* property to false  
- sets the *`currentUser`* to null.  
  
  
**Note :**  This module must be testable. In the long term, we will want to be able to pass in a mock database for testing purposes. For now, it is ok to test the module using the hard-coded list of users.