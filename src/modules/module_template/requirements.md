## Sample Module Requirements

Module should export an object which:

1. Has a **`name`** property which defaults to "Tom".

2. Has a **`sayName`** method which:  
    \- if no parameters passed returns: "Hello, my name is Tom!"  
    \- if name is passed as parameter, returns: "Hello, my name is `<name>`!"  